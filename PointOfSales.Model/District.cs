﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PointOfSales.Model
{
    [Table("MST_DISTRICT")]
    public class District
    {
        public int ID { get; set; }

        public int? RegionID { get; set; }

        [StringLength(50)]
        public string DistrictName { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        /*
        public List<Customer> Customers { get; set; }
        */
    }
}