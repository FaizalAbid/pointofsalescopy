﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointOfSales.Model
{
    [Table("TRX_PURCHASE_ORDER_DETAIL")]
    public class PurchaseOrderDetail
    {
        public int ID { get; set; }

        public int? HeaderID { get; set; }

        public int? VariantID { get; set; }

        public int? Quantity { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? SubTotal { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
