﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointOfSales.Model
{
    [Table("MST_EMPLOYEE_OUTLET")]
    public class EmployeeOutlet
    {
        public int ID { get; set; }

        public int? EmployeeID { get; set; }

        public int? OutletID { get; set; }

        public int? RoleID { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

    }
}
