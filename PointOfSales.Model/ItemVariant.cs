﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointOfSales.Model
{
    [Table("MST_ITEMS_VARIANT")]
    public class ItemVariant
    {
        public int ID { get; set; }

        public int? ItemID { get; set; }

        public int? OutletID { get; set; }

        [StringLength(255)]
        public string VariantName { get; set; }

        [StringLength(50)]
        public string SKU { get; set; }

        public decimal? Price { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        // Data Relationship
        
    }
}
