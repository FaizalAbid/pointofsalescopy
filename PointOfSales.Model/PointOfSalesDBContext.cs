﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointOfSales.Model
{
    public class PointOfSalesDBContext : DbContext
    {
        public PointOfSalesDBContext() : base("name=posConnString") // nama connection String pada app.config, test only
        {

        }

        public DbSet<AdjustmentStock> AdjustmentStock { get; set; }

        public DbSet<AdjustmentStockDetail> AdjustmentStockDetail { get; set; }

        public DbSet<Category> Category { get; set; }

        public DbSet<Customer> Customer { get; set; }

        public DbSet<District> District { get; set; }

        public DbSet<Employee> Employee { get; set; }

        public DbSet<EmployeeOutlet> EmployeeOutlet { get; set; }

        public DbSet<Item> Item { get; set; }

        public DbSet<ItemInventory> ItemInventory { get; set; }

        public DbSet<ItemVariant> ItemVariant { get; set; }

        public DbSet<Outlet> Outlet { get; set; }

        public DbSet<Payment> Payment { get; set; }

        public DbSet<PaymentDetail> PaymentDetail { get; set; }

        public DbSet<PaymentOption> PaymentOption { get; set; }

        public DbSet<Province> Province { get; set; }

        public DbSet<PurchaseOrder> PurchaseOrder { get; set; }

        public DbSet<PurchaseOrderDetail> PurchaseOrderDetail { get; set; }

        public DbSet<PurchaseOrderHistory> PurchaseOrderHistory { get; set; }

        public DbSet<PurchaseOrderStatus> PurchaseOrderStatus { get; set; }

        public DbSet<Region> Region { get; set; }

        public DbSet<Role> Role { get; set; }

        public DbSet<Supplier> Supplier { get; set; }

        public DbSet<TransferStock> TransferStock { get; set; }

        public DbSet<TransferStockDetail> TransferStockDetail { get; set; }
    }
}
