﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PointOfSales.Model
{
    [Table("MST_CATEGORIES")]
    public class Category
    {
        // construktor, dengan default createdOn
        // possible: buat prop CreatedBy read-only { get; }
        public Category()
        {
            CreatedOn = DateTime.Now;
        }

        public int ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? OutletID { get; set; }

        // status delete diganti dengan Enabled
        public bool? Enabled { get; set; }

        // Data Relationships, 
        public List<Item> Items { get; set; }

    }
}
