﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PointOfSales.Model;
using PointOfSales.DAL;


namespace PointOfSales.Web.Services
{
    /// <summary>
    /// Summary description for MasterItem
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MasterItem : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        // fix Model, conflict
        public List<PointOfSales.Model.Item> GetDataSearch(string search)
        {
            return ListItemDAL.GetItemSearch(search);
        }

        // fix Model, conflict
        public bool InsertItem(PointOfSales.Model.Item brng)
        {
            return ListItemDAL.InsertItem(brng);
        }
    }
}
