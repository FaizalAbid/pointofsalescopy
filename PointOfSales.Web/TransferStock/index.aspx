﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="PointOfSales.Web.TransferStock.index2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container">
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2>Transfer Stock</h2>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">&nbsp;</div>
                        <div class="col-md-6">
                            <form>
                                <div class="form-group">
                                    <p class="help-block">CHOOSE OUTLET</p>
                                    <hr />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">From</label>
                                    <select class="form-control" id="cb-transfer-stock-from">
                                        <option>Outlet1</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">To</label>
                                    <select class="form-control" id="cbtransfer-stock-to">
                                        <option>Outlet1</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Note" id="note-stock-transfer"></textarea>
                                </div>
                                <div class="col-md-12">&nbsp;</div>
                                <div class="form-group">
                                    <p class="help-block">TRANSFER STOCK</p>
                                    <hr />
                                </div>
                                <input type="button" class="btn btn-primary" style="width: 100%;" value="Add Stock to Transfer" />
                            </form>
                        </div>
                        <div class="col-md-3">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
