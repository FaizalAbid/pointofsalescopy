﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PointOfSales.Model;

namespace PointOfSales.DAL
{
    public class CategoryDAL
    {
        // Mengambil semua list category dari database, dengan Status Enabled == TRUE
        public static List<Category> GetCategory()
        {
            List<Category> result = new List<Category>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                // Kode untuk pertama kali generate table pada database
                //Category catToAdd = new Category
                //{
                //    Name = "Elektronik",
                //};

                //db.Category.Add(catToAdd);
                //db.SaveChanges();
                result = db.Category.ToList();
            }

            return result;
        }

        // Mengambil category dengan ID tertentu (overload GetCategory())
        public static List<Category> GetCategory(int id)
        {
            List<Category> result = new List<Category>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = db.Category.Where(x => x.ID == id).ToList();
            }

            return result;
        }

        // Insert category (tidak digabung bersama Update category)
        public static bool InsertCategory(Category cat)
        {
            bool result = false;

            using (var db = new PointOfSalesDBContext())
            {
                // set createdOn dengan NOW
                cat.CreatedOn = DateTime.Now;

                try
                {
                    db.Category.Add(cat);
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    // Todo: logs, etc...
                    // throw;
                }

            }

            return result;
        }

        // Update category, dibuat return bool untuk mereturn berhasil apa error
        public static bool UpdateCategory(Category category)
        {
            bool result = false;

            using (var db = new PointOfSalesDBContext())
            {
                // Retrieve category yang akan diupdate
                Category categoryToUpdate = db.Category.FirstOrDefault(cat => cat.ID == category.ID);

                // cek jika null
                if (categoryToUpdate != null)
                {
                    categoryToUpdate.Name = category.Name;
                    categoryToUpdate.OutletID = category.OutletID;
                    categoryToUpdate.CreatedBy = category.CreatedBy;
                    categoryToUpdate.CreatedOn = category.CreatedOn;
                    categoryToUpdate.ModifiedBy = category.ModifiedBy;
                    // set modifiedOn datetime
                    categoryToUpdate.ModifiedOn = DateTime.Now;

                    try
                    {
                        // Set state Category menjadi Modified
                        db.Entry(categoryToUpdate).State = System.Data.Entity.EntityState.Modified;
                        // Save db pada EF
                        db.SaveChanges();
                        // Set status jika berhasil
                        result = true;
                    }
                    catch (Exception)
                    {
                        // throw;
                    }
                }

            }

            return result;
        }

        // Delete category, dibuat bool untuk return berhasil apa error, Enabled = FALSE
        public static bool DeleteCategory(int id)
        {
            bool status = false;

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                Category CatToDelete = db.Category.FirstOrDefault(cat => cat.ID == id);

                // set Enabled menjadi FALSE
                if (CatToDelete != null)
                {
                    // PSEUDO-DELETE
                    CatToDelete.Enabled = false;

                    try
                    {
                        db.Entry(CatToDelete).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        status = true;
                    }
                    catch (Exception)
                    {
                        // TBA : Logs, Error, etc
                        // throw;
                    }
                }

            }

            return status;
        }

        // Search by key of categoryName
        public static List<Category> SearchCategory(string key)
        {
            List<Category> result = new List<Category>();

            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                // Can be more simple?, search: DbContext, return object result
                result = db.Category.Where(cat => cat.Name.Contains(key)).ToList();
            }

            return result;
        }
    }
}
