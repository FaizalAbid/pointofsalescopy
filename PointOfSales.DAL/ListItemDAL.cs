﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PointOfSales.Model;

namespace PointOfSales.DAL
{
    public class ListItemDAL
    {
        //DAL untuk meload data keseluruhan (input = false) dan search (input = true) 
        public static List<Item> GetItemSearch(string key)
        {
            //var result = new List<Item>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                var result=(from barang in db.Item
                        where barang.Name.Contains(key)
                        select new
                        {
                            Name = barang.Name,
                            ID = barang.ID,
                            CategoryID = barang.CategoryID,
                            ModifiedBy = barang.ModifiedBy,
                            ModifiedOn = barang.ModifiedOn,
                            CreatedBy = barang.CreatedBy,
                            CreatedOn = barang.CreatedOn,
                        });
                var resulte = result.ToList().Select(x => new Item
                        {
                            Name = x.Name,
                            ID = x.ID,
                            CategoryID = x.CategoryID,
                            ModifiedBy = x.ModifiedBy,
                            ModifiedOn = x.ModifiedOn,
                            CreatedBy = x.CreatedBy,
                            CreatedOn = x.CreatedOn,
                        }).ToList();

                return resulte;
            }
           
        }

       
        // DAL untuk Insert dan Edit item dan save ke database
        public static bool InsertItem(Item barang)
        {
            bool result = false;

            var it = new Item();
            it.ID = barang.ID;
            it.Name = barang.Name;
            it.CategoryID = barang.CategoryID;
            it.ModifiedBy = barang.ModifiedBy;
            it.ModifiedOn = barang.ModifiedOn;
            it.CreatedBy = barang.CreatedBy;
            it.CreatedOn = barang.CreatedOn;
            try
            {


                using (PointOfSalesDBContext db = new PointOfSalesDBContext())
                {
                    var ItemExsist = (from i in db.Item
                                      where i.ID == barang.ID
                                      select i).SingleOrDefault<Item>();
                    if (ItemExsist != null)
                    {
                        ItemExsist.ID = barang.ID;
                        ItemExsist.Name = barang.Name;
                        ItemExsist.CategoryID = barang.CategoryID;
                        ItemExsist.ModifiedBy = barang.ModifiedBy;
                        ItemExsist.ModifiedOn = barang.ModifiedOn;
                        ItemExsist.CreatedBy = barang.CreatedBy;
                        ItemExsist.CreatedOn = barang.CreatedOn;
                        result = true;
                    }
                    else
                    {
                        db.Item.Add(it);
                        result = true;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
    }
}
