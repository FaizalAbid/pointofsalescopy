﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using PointOfSales.Model;

namespace PointOfSales.DAL
{
    public class SupplierDAL
    {
        // Get Semua Supplier
        public static List<Supplier> GetSupplier()
        {
            List<Supplier> result = new List<Supplier>();

            using (var db = new PointOfSalesDBContext())
            {
                result = db.Supplier.ToList();
            }

            return result;
        }

        // Get Supplier by ID, overload GetSupplier()
        public static List<Supplier> GetSupplier(int id)
        {
            List<Supplier> result = new List<Supplier>();

            using (var db = new PointOfSalesDBContext())
            {
                result = db.Supplier
                    .Where(supp => supp.ID == id)
                    .ToList();
            }

            return result;
        }

        // Edit Supplier
        public static bool UpdateSupplier(Supplier supp)
        {
            bool result = false;

            using (var db = new PointOfSalesDBContext())
            {
                Supplier SupToUpdate = db.Supplier.FirstOrDefault(s => s.ID == supp.ID);

                if (SupToUpdate != null)
                {
                    SupToUpdate.Name = supp.Name;
                    SupToUpdate.Phone = supp.Phone;
                    SupToUpdate.PostalCode = supp.PostalCode;
                    SupToUpdate.ModifiedBy = supp.ModifiedBy;
                    SupToUpdate.Address = supp.Address;
                    SupToUpdate.DistrictID = SupToUpdate.DistrictID;
                    SupToUpdate.Email = supp.Email;
                    // Supplier Modified on
                    SupToUpdate.ModifiedOn = DateTime.Now;

                    try
                    {
                        db.Entry(SupToUpdate).State = EntityState.Modified;
                        db.SaveChanges();
                        // set to true
                        result = true;
                    }
                    catch (Exception)
                    {
                        // todo: logs
                    }
                }
            }

            return result;
        }

        // Delete supplier?
        //public static bool DeleteSupplier(int id)
        //{
        //    bool result = false;


        //    return result;
        //}

        // search supplier by key
        public static List<Supplier> SearchSupplier(string key)
        {
            List<Supplier> result = new List<Supplier>();

            using (var db = new PointOfSalesDBContext())
            {
                result = db.Supplier.Where(supp => supp.Name.Contains(key)).ToList();
            }

            return result;
        }
    }
}
