﻿using PointOfSales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PointOfSales.DAL
{
    class LoginDAL
    {
        public static List<TransferStock> GetTransferStock()
        {
            List<TransferStock> result = new List<TransferStock>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = db.TransferStock.Select(
                    anu => new TransferStock
                    {
                        ID = anu.ID,
                        FromOutlet = anu.FromOutlet,
                        Note = anu.Note,
                        ToOutlet = anu.ToOutlet
                    }).ToList();
            }
            return result;
        }
    }
}
