﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PointOfSales.Model;

namespace PointOfSales.DAL
{
    public class LoadItemDAL
    {
        public static List<Item> GetItemString(string key)
        {
            List<Item> result = new List<Item>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from Barang in db.Item
                          where Barang.Name
                          select new Item
                          {
                              Name = Barang.Name
                          }).ToList();
            }
            return result;
        }
    }
}
