﻿using PointOfSales.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PointOfSales.Model;

namespace PointOfSales.DAL
{
    public class PurchaseOrderDAL
    {
        public static List<PurchaseOrder> GetPO()
        {
            List<PurchaseOrder> result = new List<PurchaseOrder>();
            using (PointOfSalesDBContext db = new PointOfSalesDBContext())
            {
                result = (from p in db.PurchaseOrder
                             select p).ToList();
                // this doesnt work gaes
                //result = db.PurchaseOrder.Select(
                //    dataPO => new PurchaseOrder()
                //    {
                //        ID = dataPO.ID,
                //        OutletID = dataPO.OutletID,
                //        SupplierID = dataPO.SupplierID,
                //        OrderNo = dataPO.OrderNo,
                //        Notes = dataPO.Notes,
                //        StatusID = dataPO.StatusID
                //    }).ToList();
            }
            return result;
        }
    }
}
